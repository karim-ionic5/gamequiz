import { Injectable } from '@angular/core';
import { GameQuestion } from '../gamequestion';
import { interval } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  // Constants for quiz operation.
  public questions: GameQuestion[] = [];
  private activeQuestion: GameQuestion;
  private isCorrect: boolean;
  private isSelected: boolean;
  private questionCounter: number;
  private optionCounter: number;
  private startTime: Date;
  private endTime: Date;
  private duration: number;
  private correctAnswers: number = 0;

  // Constants for individual question timer, and for points calculation.
  private timeLeft: number;
  private interval: any;
  private pointsCounter: number;
  public totalPoints: number = 0;

  constructor() { }

  // Set all questions for quiz which are presented in order as user progresses.
  public addQuestions() {
    this.questions = [{
      image: "../../assets/img/1.jpg",
      options: ['Aloy', 'Liza', 'Zima', 'Elilah'],
      correctOption: 0
    },
    {
      image: '../../assets/img/2.jpg',
      options: ['Qin Wu Xian', 'Tadashi Tsumura', 'Jin Sakai', 'Keichi Nakamura'],
      correctOption: 2
    },
    {
      image: '../../assets/img/3.jpg',
      options: ['Strangelove', 'November "Nova" Terra', 'Ariel Hanson', 'October "Octa" Tanismoore'],
      correctOption: 1
    },
    {
      image: '../../assets/img/4.jpg',
      options: ['Akira Kurusu aka "Joker"', 'Naoto Shirogane aka "Nashi"', 'Ken Amada aka "Blackstar"',
        'Honda Shirane aka "Detective"'],
      correctOption: 0
    },
    {
      image: '../../assets/img/5.jpg',
      options: ['Lily Tapper', 'Elena Bancroft', 'Sarah Bright', 'Lara Croft'],
      correctOption: 3
    },
    {
      image: '../../assets/img/6.jpg',
      options: ['Khalid and Boo', 'Minsc and Boo', 'Brody and Boo', 'Adam and Boo'],
      correctOption: 1
    },
    {
      image: '../../assets/img/7.jpg',
      options: ['Sgt. Stone', 'Sgt. Warhammer', 'Sgt. Axe', 'Sgt. Hammer'],
      correctOption: 3
    },
    {
      image: '../../assets/img/8.jpg',
      options: ['Cullen Rutherford', 'Thomas Coventry', 'Hugh Frankland', 'Gerard Lumley'],
      correctOption: 0
    },
    {
      image: '../../assets/img/9.jpg',
      options: ['Bella Winters', 'Samantha Hicks', 'Amanda Ripley', 'Heather Mason'],
      correctOption: 2
    },
    {
      image: '../../assets/img/10.jpg',
      options: ['CO Malcolm Wright', 'CO Ian McKinley', 'CO John Bradford', 'CO Dean Winchester'],
      correctOption: 2
    },
    {
      image: '../../assets/img/11.jpg',
      options: ['Nassana Dantius', 'Liara T\'soni', 'Ygara Menoris', 'Aenea Endymion'],
      correctOption: 1
    },
    {
      image: '../../assets/img/12.jpg',
      options: ['JC Denton', 'Morgan Blackhand', 'Johnny Silverhand', 'Adam Jensen'],
      correctOption: 3
    },
    ];
  }

  // Set active question and make sure selections are cleared.
  public setQuestion() {
    this.isCorrect = false;
    this.isSelected = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
    this.startTimer();
  }

  // Reset question counter, set start time for quiz and get first question.
  public initialize() {
    this.totalPoints = 0;
    this.correctAnswers = 0;
    this.questionCounter = 0;
    this.startTime = new Date();
    this.setQuestion();
  }

  // Get game questions.
  public getQuestions(): GameQuestion[] {
    return this.questions;
  }

  // Get active question.
  public getActiveQuestion(): GameQuestion {
    return this.activeQuestion;
  }

  // Get the portrait image for active question.
  public getPortraitOfActiveQuestion(): string {
    return this.activeQuestion.image;
  }

  // Get options for active question.
  public getOptionsOfActiveQuestion(): string[] {
    return this.activeQuestion.options;
  }

  // Get the order number of active question.
  public getIndexOfActiveQuestion(): number {
    return this.questionCounter;
  }

  // How many questions are there in total?
  public getNumberOfQuestions(): number {
    return this.questions.length;
  }

  // Get the number of options for active question.
  public getNumberOfQuestionsOfActiveQuestion(): number {
    return this.activeQuestion.options.length;
  }

  // Get the index of options.
  public getIndexOfOptionCounter() {
    return this.optionCounter;
  }

  // Get the correct option of active question.
  public getCorrectOptionOfActiveQuestion(): string {
    return this.activeQuestion.options[this.activeQuestion.correctOption];
  }

  // Set an option as selected.
  public setOptionSelected() {
    this.isSelected = true;
  }

  // Is an option selected?
  public isOptionSelected(): boolean {
    return this.isSelected;
  }

  // Is the selected answer correct?
  public isCorrectOption(option: number) {
    this.isCorrect = (option === this.activeQuestion.correctOption) ? true : false;
    if (option === this.activeQuestion.correctOption === true) {
      this.correctAnswers++;
    }
    return this.isCorrect;
  }

  // Is the answer correct?
  public isAnswerCorrect(): boolean {
    return this.isCorrect;
  }

  // Is the quiz finished?
  public isFinished(): boolean {
    return (this.questionCounter === this.questions.length) ? true : false;
  }

  // Get time used for whole quiz and return it.
  public getDuration(): number {
    this.endTime = new Date();
    this.duration = this.endTime.getTime() - this.startTime.getTime();
    return this.duration;
  }

  // Start question timer.
  public startTimer() {
    this.timeLeft = 30;
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.timeLeft = 0;
      }
    }, 1000)
  }

  // Stop question timer.
  public stopTimer() {
    clearInterval(this.interval);
  }

  // Count timer time left, and points gained from correctly answered question based on time left and add them to total
  // point value. Finally, return total point value.
  public countPoints() {
    if (this.timeLeft > 20) {
      this.pointsCounter = 5;
    } else if (this.timeLeft > 10 && this.timeLeft <= 20) {
      this.pointsCounter = 3;
    } else {
      this.pointsCounter = 1;
    }
    this.totalPoints = this.totalPoints + this.pointsCounter;
    return this.totalPoints;
  }

  // Play audio function.
  public playAudio(){
    let audio = new Audio();
    audio.src = "assets/sound/" + this.getIndexOfActiveQuestion() + "quote.mp3";
    audio.volume = 1;
    audio.load();
    audio.play();
  }
}
