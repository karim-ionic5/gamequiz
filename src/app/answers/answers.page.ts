import { Component, OnInit } from '@angular/core';
import { QuizService} from '../quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.page.html',
  styleUrls: ['./answers.page.scss'],
})
export class AnswersPage implements OnInit {

  constructor(private quizService: QuizService, private router: Router ) { }

  ngOnInit() {
  }
  // Return to quiz home page and initialize.
  private goHome() {
    this.quizService.initialize();
    this.router.navigateByUrl('home');
  }
}
