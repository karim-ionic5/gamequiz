import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuizService } from '../quiz.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {


  constructor(public router: Router, private quizService: QuizService) { }

  // Start quiz.
  start() {
    this.router.navigateByUrl('question');
  }

  answers() {
    this.router.navigateByUrl('answers');
  }
}
