import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QuizService } from '../quiz.service';


@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {
  // Constants for quiz duration.
  private duration: number;
  private durationSecs: number;

  // Constants for feedback.
  private feedback: string;
  private feedbackimg: any;

  constructor(public router: Router, public activatedRoute: ActivatedRoute, private quizService: QuizService) { }

  // Get quiz duration.
  ngOnInit() {
    this.duration = Number(this.activatedRoute.snapshot.paramMap.get('duration'));
    this.durationSecs = Math.round((this.duration) / 1000);
    console.log(this.ngHide());
  }

  // Set feedback and image according to user score.
  ngHide() {
    if (this.quizService.totalPoints === 60) {
      this.feedback = "You are a gaming master!"
      this.feedbackimg ="../../assets/img/bestscore.jpg"
    } else if (this.quizService.totalPoints >= 50 && this.quizService.totalPoints < 60) {
      this.feedback = "You sure know your games. Kudos!"
      this.feedbackimg ="../../assets/img/goodscore.jpg"
    } else if (this.quizService.totalPoints < 50 && this.quizService.totalPoints >= 25) {
      this.feedback = "Not bad, play some more games!"
      this.feedbackimg ="../../assets/img/averagescore.jpg"
    } else if (this.quizService.totalPoints < 25 && this.quizService.totalPoints > 5) {
      this.feedback = "At least you know the concept..."
      this.feedbackimg ="../../assets/img/lowscore.jpg"
    } else {
      this.feedback = "V-i-d-e-o g-a-m-e-s....google it!"
      this.feedbackimg ="../../assets/img/worstscore.jpg"
    }
  }

  // Stop timer to prevent stacking. Return to quiz home page and initialize.
  public goHome() {
    this.quizService.stopTimer();
    this.quizService.initialize();
    this.router.navigateByUrl('home');
  }

  answers() {
    this.router.navigateByUrl('answers');
  }
}
