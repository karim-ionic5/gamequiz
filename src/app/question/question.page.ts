import { Component, OnInit } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {
  // Constants for feedback and duration.
  private feedback: string;
  private duration: number;

  constructor(public router: Router, private quizService: QuizService) { }

  // Make sure that the timer is stopped to prevent stackingSetup all questions for the quiz and present the first 
  // question.
  ngOnInit() {
    this.quizService.stopTimer();
    this.quizService.addQuestions();
    this.quizService.initialize();
    this.feedback = "";
  }

  // Check if selected option is correct and stop question timer. If the answer is correct, calculate points and play
  // character audio quote. If not, no points are added. In both cases user receives feedback string.
  private checkOption(option: any) {
    this.quizService.setOptionSelected();
    this.quizService.stopTimer();
    if (this.quizService.isCorrectOption(option)) {
      this.quizService.countPoints();
      this.quizService.playAudio();
      this.feedback = this.quizService.getCorrectOptionOfActiveQuestion() + ' is correct! Go on...';
    }
    else {
      this.feedback = 'That is incorrect!';

    }
  }

// Continue the quiz if the quiz is not deemed finished, otherwise show result page with quiz duration and stop the
// timer (to prevent stacking) in case user didn't select an option and just clicked continue.
  private continue() {
    if (this.quizService.isFinished()) {
      this.duration = this.quizService.getDuration();
      this.router.navigateByUrl(`result/${this.duration}`);
    } else {
      this.quizService.stopTimer();
      this.quizService.setQuestion();
    }
  }
}
