export class GameQuestion {
    image?: string;
    options: string[];
    correctOption: number;
}